require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

Vue.component('communication-component', require('./components/CommunicationComponent.vue'));

const app = new Vue({
    el: '#app',
    data: {

    },

    methods: {
        parentMethod(){
            alert('Parent received notification from communication component!')
        }
    }
});